#!/bin/bash
#Currently only works when it is executed 

NEXTCLOUD_HOME="/services/nextcloud"
NEXTCLOUD_PORT=80
NEXTCLOUD_HTTPS=443
SSHPORT=22
POSTGRESQL_PORT=5432
POSTGRESQL_PASSWORD="gw94kg9wkpgkkfdkgrewög39"

for i in $*; do
  prefix=`echo $i | cut -c1-2`
  case "$prefix" in
    -h) NEXTCLOUD_HOME=`echo ${i:2}`;;
    -n) NEXTCLOUD_PORT=`echo ${i:2}`;;
    -s) NEXTCLOUD_HTTPS`echo ${i:2}`;;
    -S) SSHPORT=`echo ${i:2}`;;
    -p) POSTGRESQL_PORT=`echo ${i:2}`;;
    -P) POSTGRESQL_PASSWORD=`echo ${i:2}`;;
  esac
done

echo "Nextcloud path: $NEXTCLOUD_HOME"
echo "Nextcloud port: $NEXTCLOUD_PORT"
echo "Nextcloud ssl port: $NEXTCLOUD_HTTPS"
echo "SSH port: $SSHPORT"
echo "PostgreSQL port: $POSTGRESQL_PORT"
echo "PostgreSQL password: $POSTGRESQL_PASSWORD"


# Enable ports
sudo firewall-cmd --zone=public --permanent --add-port ${NEXTCLOUD_PORT}/tcp
sudo firewall-cmd --zone=public --permanent --add-port ${POSTGRESQL_PORT}/tcp
sudo firewall-cmd --reload


sudo mkdir $NEXTCLOUD_HOME 
cd $NEXTCLOUD_HOME && \
sudo mkdir -p html apps config data theme

# Create Nextcloudserver Pod
podman pod create --name nextcloud_server  \
  --publish $NEXTCLOUD_HTTPS:443 --publish $NEXTCLOUD_PORT:80 --publish $SSHPORT:22 \
  --publish $POSTGRESQL_PORT:5432

# Postgres Database for Nextcloud
cd $NEXTCLOUD_HOME && \
sudo mkdir -p database

podman run -dt --pod nextcloud_server \
  --name nextcloud_postgres \
  -e POSTGRES_PASSWORD=$POSTGRESQL_PASSWORD \
  -e POSTGRES_USER=nextcloud \
  -e POSTGRES_DB=nextcloud \
  -v $NEXTCLOUD_HOME/database:/var/lib/postgresql/data:Z \
  -d postgres

# Nextcloud
podman run -dt --pod nextcloud_server \
  --name nextcloud \
  --restart always \
  -v $NEXTCLOUD_HOME/html:/var/www/html:Z \
  -v $NEXTCLOUD_HOME/apps:/var/www/html/custom_apps:Z \
  -v $NEXTCLOUD_HOME/config:/var/www/html/config:Z \
  -v $NEXTCLOUD_HOME/data:/var/www/html/data:Z \
  -v $NEXTCLOUD_HOME/theme:/var/www/html/themes:Z \
nextcloud



